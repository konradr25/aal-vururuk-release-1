package main.java.konradr25.aal.fileparser;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import main.java.konradr25.aal.model.people.Man;
import main.java.konradr25.aal.model.people.Woman;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.stream.Stream;

/**
 * Helper class that parses files
 */
public class FileParser {

    private LinkedList<Man> menList;
    private LinkedList<Woman> womenList;

    public FileParser(String filePath) {
        menList = Lists.newLinkedList();
        womenList = Lists.newLinkedList();

        Path path = FileSystems.getDefault().getPath(filePath);

        try {
            final Stream<String> lines = Files.lines(path);
            lines.forEach(line -> parseSingleLine(line));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void parseSingleLine(String line) {
        //wzrost mezczyzny, rozmiar jego glowy, wzrost kobiety, rozmiar jej glowy
        final Iterable<String> split = Splitter.on(' ').split(line);
        final String stringToFirstSpace = Iterables.get(split, 0);
        if (stringToFirstSpace == null || stringToFirstSpace.isEmpty() || stringToFirstSpace.charAt(0) == '#') {
            return;
        }
        final String manHeight = stringToFirstSpace;
        final String manHead = Iterables.get(split, 1);
        final String womanHeight = Iterables.get(split, 2);
        final String womanHead = Iterables.get(split, 3);

        final Man man = new Man(Integer.parseInt(manHead), Integer.parseInt(manHeight));
        final Woman woman = new Woman(Integer.parseInt(womanHead), Integer.parseInt(womanHeight));

        menList.add(man);
        womenList.add(woman);
    }

    /**
     * Gets menList.
     *
     * @return the menList
     */
    public LinkedList<Man> getMenList() {
        return menList;
    }

    /**
     * Gets womenList.
     *
     * @return the womenList
     */
    public LinkedList<Woman> getWomenList() {
        return womenList;
    }
}
