package main.java.konradr25.aal.common.Utils;

/**
 * Helper class.
 */
public class FilesUtils {

    /**
     * Gets slash depending on os version.
     *
     * @return slash
     */
    public static String getSlash() {
        if(OsUtils.isWindows()){
            return "\\";
        } else {
            return "/";
        }
    }
}
