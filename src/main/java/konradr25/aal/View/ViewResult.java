package main.java.konradr25.aal.View;

import javax.swing.*;
import java.util.LinkedHashMap;
import java.util.Map;

public class ViewResult extends Thread {
    private final LinkedHashMap<Integer, Long> numberOfPeopleToTime;
    private JTextArea nTextArea;
    private JTextArea tnTextArea;
    private JPanel rightJPanel;
    private JPanel leftJPanel;
    private JPanel mainJPanel;
    private JScrollPane jScrollPane;


    public ViewResult(LinkedHashMap<Integer, Long> numberOfPeopleToTime) {
        this.numberOfPeopleToTime = numberOfPeopleToTime;
    }


    private void createForm() {
        JFrame frame = new JFrame("ViewResult");
        frame.setContentPane(mainJPanel);
        frame.setSize(400,600);
        frame.pack();
        frame.setVisible(true);
    }

    public void run() {
        createForm();
        addAllDataToTextAreas();
    }

    private void addAllDataToTextAreas() {

        for ( Map.Entry<Integer, Long> entry: numberOfPeopleToTime.entrySet()) {
            nTextArea.append(entry.getKey().toString() + "\n");
            tnTextArea.append(entry.getValue().toString() + "\n");
        }

    }

}
