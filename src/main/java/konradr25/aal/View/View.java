package main.java.konradr25.aal.View;

import com.google.common.collect.Maps;
import main.java.konradr25.aal.common.MainParametersType;
import main.java.konradr25.aal.common.RandomParameterType;
import main.java.konradr25.aal.events.ApplicationEvent;
import main.java.konradr25.aal.events.StartAlgorithmEvent;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

public class View {
    private JButton startButton;
    private JRadioButton fromFileRadioButton;
    private JRadioButton randomRadioButton;
    private JTextField initialNumberOfPeopleTextField;
    private JTextField minWomanHeadtextField;
    private JTextField maxWomanHeadTextField;
    private JRadioButton mojWlasnyRadioButton;
    private JPanel rightJPanel;
    private JPanel leftJPanel;
    private JPanel paramsRandomGenerationJPanel;
    private JPanel headerRightJPanel;
    private JPanel mainJPanel;
    private JTextField filePathTextField;
    private JLabel numberOfPeopleJLabel;
    private JLabel minWomanHeadLabel;
    private JTextField minManHeadDifferenceTextField;
    private JTextField maxManHeadDifferenceTextField;
    private JTextField minManHeightDifferenceTextField;
    private JTextField maxManHeightDifferenceTextField;
    private JLabel maxWomanHeadLabel;
    private JLabel minManHeadDifference;
    private JTextField minWomanHeightTextField;
    private JTextField maxWomanHeightTextField;
    private JLabel maxManHeightDifferenceJLabel;
    private JLabel minManHeightDifferenceJLabel;
    private JLabel maxManHeadDifferenceJLabel;
    private JLabel minWomanHeightJLabel;
    private JLabel maxWomanHeightJLabel;
    private JLabel increasesByXEveryIterationJPanel;
    private JTextField incrementValueTextField;
    private JLabel numberOfIterationsJPanel;
    private JTextField numberOfIterationsTextField;
    private BlockingQueue<ApplicationEvent> eventsBlockingQueue;

    private HashMap<RandomParameterType, JTextField> randomParameterTypeJTextFieldHashMap;

    public View(BlockingQueue<ApplicationEvent> eventsBlockingQueue) {
        this.eventsBlockingQueue = eventsBlockingQueue;
        randomParameterTypeJTextFieldHashMap = Maps.newHashMap();

        createRandomParameterTypeJTextFieldHashMap();

        createFrame();


    }

    private void createRandomParameterTypeJTextFieldHashMap() {
        randomParameterTypeJTextFieldHashMap.put(RandomParameterType.INITIAL_NUMBER_OF_PEOPLE, initialNumberOfPeopleTextField);
        randomParameterTypeJTextFieldHashMap.put(RandomParameterType.MEN_MAX_HEAD_DIFFERENCE, maxManHeadDifferenceTextField);
        randomParameterTypeJTextFieldHashMap.put(RandomParameterType.MEN_MAX_HEIGHT_DIFFERENCE, maxManHeightDifferenceTextField);
        randomParameterTypeJTextFieldHashMap.put(RandomParameterType.MEN_MIN_HEAD_DIFFERENCE, minManHeadDifferenceTextField);
        randomParameterTypeJTextFieldHashMap.put(RandomParameterType.MEN_MIN_HEIGHT_DIFFERENCE, minManHeightDifferenceTextField);
        randomParameterTypeJTextFieldHashMap.put(RandomParameterType.WOMEN_MAX_HEAD, maxWomanHeadTextField);
        randomParameterTypeJTextFieldHashMap.put(RandomParameterType.WOMEN_MIN_HEAD, minWomanHeadtextField);
        randomParameterTypeJTextFieldHashMap.put(RandomParameterType.WOMEN_MAX_HEIGHT, maxWomanHeightTextField);
        randomParameterTypeJTextFieldHashMap.put(RandomParameterType.WOMEN_MIN_HEIGHT, minWomanHeightTextField);
        randomParameterTypeJTextFieldHashMap.put(RandomParameterType.INCREMENT_VALUE, incrementValueTextField);
        randomParameterTypeJTextFieldHashMap.put(RandomParameterType.NUMBER_OF_ITERATIONS, numberOfIterationsTextField);

    }

    private void createFrame() {
        JFrame frame = new JFrame("View");
        frame.setContentPane(mainJPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        ButtonGroup group = new ButtonGroup();
        group.add(fromFileRadioButton);
        group.add(randomRadioButton);
        setTextFieldsEditableDependingOnButtons();
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                createEventAfterPressingStartButton();
            }
        });
        randomRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setTextFieldsEditableDependingOnButtons();
            }
        });
        fromFileRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setTextFieldsEditableDependingOnButtons();
            }
        });

        frame.setVisible(true);
    }

    private void setTextFieldsEditableDependingOnButtons() {
        final boolean selectedRandom = randomRadioButton.isSelected();
        randomParameterTypeJTextFieldHashMap.values().forEach(value -> value.setEditable(selectedRandom));

        filePathTextField.setEditable(fromFileRadioButton.isSelected());
    }

    private void createEventAfterPressingStartButton() {
        HashMap<Boolean, String> fromFile = Maps.newHashMap();
        fromFile.put(fromFileRadioButton.isSelected(), filePathTextField.getText());
        final HashMap<RandomParameterType, Integer> randomParametersFromFrame;
        final HashMap<MainParametersType, Boolean> mainParametersFromFrame;

        if (randomRadioButton.isSelected()) {
            randomParametersFromFrame = getRandomParametersFromFrame();
            mainParametersFromFrame = getMainParametersFromFrame();
        } else {
            randomParametersFromFrame = Maps.newHashMap();
            mainParametersFromFrame = Maps.newHashMap();
        }



        try {
            final StartAlgorithmEvent startAlgorithmEvent = new StartAlgorithmEvent(fromFile,
                    randomParametersFromFrame, mainParametersFromFrame);
            eventsBlockingQueue.put(startAlgorithmEvent);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private boolean areRandomTextFieldsNotEmpty() {
       return !randomParameterTypeJTextFieldHashMap.values().stream()
                .anyMatch(value -> value.getText() == null || value.getText().isEmpty());



    }
    private HashMap<RandomParameterType, Integer> getRandomParametersFromFrame() {
        HashMap<RandomParameterType, Integer> randomParameterTypeToValue = Maps.newHashMap();

        if (areRandomTextFieldsNotEmpty()) {

            for ( Map.Entry<RandomParameterType, JTextField> entry : randomParameterTypeJTextFieldHashMap.entrySet()) {
                randomParameterTypeToValue.put(entry.getKey(), Integer.parseInt(entry.getValue().getText()));
            }
        }

        return randomParameterTypeToValue;
    }

    private HashMap<MainParametersType, Boolean> getMainParametersFromFrame() {
        HashMap<MainParametersType, Boolean> mainParametersTypeToValue = Maps.newHashMap();
        mainParametersTypeToValue.put(MainParametersType.IS_DATA_RANDOM_GENERATED, randomRadioButton.isSelected());
        mainParametersTypeToValue.put(MainParametersType.IS_MY_ALGORITHM, mojWlasnyRadioButton.isSelected());
        return mainParametersTypeToValue;
    }
}


