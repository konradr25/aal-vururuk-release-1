package main.java.konradr25.aal.View;

import main.java.konradr25.aal.model.people.MyPair;

import javax.swing.*;
import java.util.HashSet;

/**
 * Created by admin on 2016-01-21.
 */
public class ViewResultPairs extends Thread {
    private JTextArea availablePairsTextArea;
    private JLabel pairsLabel;
    private JPanel jPanel;
    private HashSet<MyPair> pairs;
    public ViewResultPairs(HashSet<MyPair> pairs) {
        this.pairs = pairs;
    }

    private void createForm() {
        JFrame frame = new JFrame("Pairs made");
        frame.setContentPane(jPanel);
        frame.setSize(200,600);
        frame.pack();
        frame.setVisible(true);
    }

    public void run() {
        createForm();
        addAllDataToTextAreas();
    }

    private void addAllDataToTextAreas() {

        for ( MyPair myPair: pairs) {
            availablePairsTextArea.append(myPair.getMan().toString() + "\t" + myPair.getWoman().toString() + "\n");
        }

    }
}
