package main.java.konradr25.aal.model.people;

public class MyPair {

    private Person man;
    private Person woman;

    public MyPair(Person man, Person woman) {
        this.man = man;
        this.woman = woman;
    }

    /**
     * Gets man.
     *
     * @return the man
     */
    public Person getMan() {
        return man;
    }

    /**
     * Sets man.
     *
     * @param man
     */
    public void setMan(Person man) {
        this.man = man;
    }

    /**
     * Gets woman.
     *
     * @return the woman
     */
    public Person getWoman() {
        return woman;
    }

    /**
     * Sets woman.
     *
     * @param woman
     */
    public void setWoman(Person woman) {
        this.woman = woman;
    }
}
