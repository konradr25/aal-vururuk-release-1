package main.java.konradr25.aal.model.people;

public class Person {
    protected Boolean isMan;
    protected Integer head;
    protected Integer height;

    public Person(Integer head, Integer height) {
        this.head = head;
        this.height = height;
        this.isMan = true;
    }

    /**
     * Gets head.
     *
     * @return the head
     */
    public Integer getHead() {
        return head;
    }

    /**
     * Sets head.
     *
     * @param head
     */
    public void setHead(Integer head) {
        this.head = head;
    }

    /**
     * Gets height.
     *
     * @return the height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * Sets height.
     *
     * @param height
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * Gets isMan.
     *
     * @return the isMan
     */
    public Boolean getMan() {
        return isMan;
    }

    /**
     * Sets isMan.
     *
     * @param man
     */
    public void setMan(Boolean man) {
        isMan = man;
    }

    @Override
    public String toString() {
        StringBuilder resultToShow = new StringBuilder();
        if (isMan) {
            resultToShow.append("Man");
        } else {
            resultToShow.append("Woman");
        }
        return resultToShow.append(" height: ").append(height.toString()).append(" head: ").append(head.toString()).toString();
    }
}
