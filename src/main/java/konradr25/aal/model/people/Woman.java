package main.java.konradr25.aal.model.people;

public class Woman extends Person {
    public Woman(Integer head, Integer height) {
        super(head, height);
        isMan = false;
    }
}
