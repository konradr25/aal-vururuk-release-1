package main.java.konradr25.aal.model.people;


public class Man extends Person {

    public Man(Integer head, Integer height) {
        super(head, height);
        isMan = true;
    }
}
