package main.java.konradr25.aal.model.people;

import java.util.Comparator;

public class PersonComparator implements Comparator<Person> {

    @Override
    public int compare(Person e1, Person e2) {
        if (e1.getHeight() > e2.getHeight()) {
            return 1;
        } else if (e1.getHeight() < e2.getHeight()) {
            return -1;
        } else {
            if (e1.getHead() > e2.getHead()) {
                return 1;
            } else if (e1.getHead() < e2.getHead()) {
                return -1;
            } else {
                return 0;
            }
        }
    }

}