package main.java.konradr25.aal.model;

import com.google.common.collect.Lists;
import main.java.konradr25.aal.common.RandomParameterType;
import main.java.konradr25.aal.model.people.Man;
import main.java.konradr25.aal.model.people.Woman;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class RandomDataGenerator {

    private LinkedList<Man> manArray;
    private LinkedList<Woman> womenArray;


    public RandomDataGenerator(HashMap<RandomParameterType, Integer> randomParametersFromFrame, Integer numberOfPeople) {

        manArray = Lists.newLinkedList();
        womenArray = Lists.newLinkedList();

        generateRandomData( randomParametersFromFrame.get(RandomParameterType.WOMEN_MAX_HEIGHT),
                randomParametersFromFrame.get(RandomParameterType.WOMEN_MIN_HEIGHT),
                randomParametersFromFrame.get(RandomParameterType.WOMEN_MAX_HEAD),
                randomParametersFromFrame.get(RandomParameterType.WOMEN_MIN_HEAD),
                randomParametersFromFrame.get(RandomParameterType.MEN_MIN_HEIGHT_DIFFERENCE),
                randomParametersFromFrame.get(RandomParameterType.MEN_MAX_HEIGHT_DIFFERENCE),
                randomParametersFromFrame.get(RandomParameterType.MEN_MIN_HEAD_DIFFERENCE),
                randomParametersFromFrame.get(RandomParameterType.MEN_MAX_HEAD_DIFFERENCE),
                numberOfPeople);
    }

    /**
     * Gets manArray.
     *
     * @return the manArray
     */
    public LinkedList<Man> getManArray() {
        return manArray;
    }

    /**
     * Gets womenArray.
     *
     * @return the womenArray
     */
    public LinkedList<Woman> getWomenArray() {
        return womenArray;
    }

    private void generateRandomData(final Integer maxWomanHeight, final Integer minWomanHeight,
                                          final Integer maxWomanHead, final Integer minWomanHead,
                                          final Integer minHeightDifference, final Integer maxHeightDifference,
                                          final Integer minHeadDifference, final Integer maxHeadDifference,
                                          final Integer numberOfPeople) {

        ExecutorService executor = Executors.newFixedThreadPool(10);

        /// women
        Callable<LinkedList<Integer>> generateWomenHeightTask = () -> {
            final GenerateRandomNumbers generateRandomNumbers = new GenerateRandomNumbers(maxWomanHeight, minWomanHeight, numberOfPeople);
            return generateRandomNumbers.getRandomNum();
        };
        Future<LinkedList<Integer>> generateWomenHeightFuture = executor.submit(generateWomenHeightTask);

        Callable<LinkedList<Integer>> generateWomenHeadTask = () -> {
            final GenerateRandomNumbers generateRandomNumbers = new GenerateRandomNumbers(maxWomanHead, minWomanHead, numberOfPeople);
            return generateRandomNumbers.getRandomNum();
        };
        Future<LinkedList<Integer>> generateWomenHeadFuture = executor.submit(generateWomenHeadTask);

        /// men difference

        Callable<LinkedList<Integer>> generateMenHeightDifferenceTask = () -> {
            final GenerateRandomNumbers generateRandomNumbers = new GenerateRandomNumbers(maxHeightDifference, minHeightDifference, numberOfPeople);
            return generateRandomNumbers.getRandomNum();
        };
        Future<LinkedList<Integer>> generateMenHeightDifferenceFuture = executor.submit(generateMenHeightDifferenceTask);

        Callable<LinkedList<Integer>> generateMenHeadDifferenceTask = () -> {
            final GenerateRandomNumbers generateRandomNumbers = new GenerateRandomNumbers(maxHeadDifference, minHeadDifference, numberOfPeople);
            return generateRandomNumbers.getRandomNum();
        };
        Future<LinkedList<Integer>> generateMenHeadDifferenceFuture = executor.submit(generateMenHeadDifferenceTask);


        try {

            final LinkedList<Integer> womenHeads = generateWomenHeadFuture.get();
            final LinkedList<Integer> womenHeights = generateWomenHeightFuture.get();

            Callable<LinkedList<Woman>> generateWomenObjects = () -> {

                for (int index = 0; index < womenHeads.size(); ++index) {
                    womenArray.add(new Woman(womenHeads.get(index), womenHeights.get(index)));
                }
                return womenArray;
            };

            Future<LinkedList<Woman>> generateWomenObjectsFuture = executor.submit(generateWomenObjects);


            final LinkedList<Integer> menHeightDifferences = generateMenHeightDifferenceFuture.get();
            final LinkedList<Integer> menHeadDifferences = generateMenHeadDifferenceFuture.get();
            final LinkedList<Woman> womansObjectsList = generateWomenObjectsFuture.get();


            for (int index = 0; index < womenHeads.size(); ++index) {
                manArray.add(new Man(womenHeads.get(index) + menHeightDifferences.get(index),
                        womenHeights.get(index) + menHeightDifferences.get(index)));
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    private class GenerateRandomNumbers {
        private LinkedList<Integer> randomNum;
        public GenerateRandomNumbers(Integer max, Integer min, Integer count) {

            randomNum = Lists.newLinkedList();
            for (int i = 0 ; i < count; ++i) {
                generateRandomNumber(max, min);
            }
        }

        private void generateRandomNumber(Integer max, Integer min) {
            Random rand = new Random();
            randomNum.add(min + rand.nextInt((max - min) + 1));
        }

        public LinkedList<Integer> getRandomNum() {
            return randomNum;
        }
    }
}
