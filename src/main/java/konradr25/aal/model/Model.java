package main.java.konradr25.aal.model;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import main.java.konradr25.aal.common.RandomParameterType;
import main.java.konradr25.aal.events.ApplicationEvent;
import main.java.konradr25.aal.events.ShowResultsEvent;
import main.java.konradr25.aal.events.StartAlgorithmEvent;
import main.java.konradr25.aal.fileparser.FileParser;
import main.java.konradr25.aal.model.people.Man;
import main.java.konradr25.aal.model.people.MyPair;
import main.java.konradr25.aal.model.people.Woman;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;


public class Model {

    LinkedList<Man> menList;
    LinkedList<Woman> womenList;
    Boolean shouldGetTestDataFromFile;
    public Model() {
    }

    public ApplicationEvent runAlgorithmModel(StartAlgorithmEvent event) {
        event.getMainParametersFromFrame();
        HashSet<MyPair> myPairs = Sets.newHashSet();
        final HashMap<Boolean, String> shouldReadFromFile = event.getShouldReadFromFile();

        shouldGetTestDataFromFile = shouldReadFromFile != null && !shouldReadFromFile.isEmpty() &&
                shouldReadFromFile.keySet().iterator().next();

        LinkedHashMap<Integer, Long> numberOfPeopleToTime = Maps.newLinkedHashMap();
        if (shouldGetTestDataFromFile) {
            getTestDataFromFile(shouldReadFromFile);
            final Long intervalTime = runMainAlgorithm(myPairs);
            numberOfPeopleToTime.put(womenList.size(), intervalTime);
        } else {

            final HashMap<RandomParameterType, Integer> randomParametersFromFrame
                    = event.getRandomParametersFromFrame();

            final Integer incrementValue = randomParametersFromFrame.get(RandomParameterType.INCREMENT_VALUE);
            final Integer numberOfIterations = randomParametersFromFrame.get(RandomParameterType.NUMBER_OF_ITERATIONS);
             Integer initialNumberOfPeople
                    = randomParametersFromFrame.get(RandomParameterType.INITIAL_NUMBER_OF_PEOPLE);

            for (int i = 0; i< numberOfIterations; ++i) {
                getRandomTestData(randomParametersFromFrame, initialNumberOfPeople);
                Long intervalTime = runMainAlgorithm(myPairs);
                numberOfPeopleToTime.put(initialNumberOfPeople, intervalTime);
                initialNumberOfPeople += incrementValue;
            }
        }

        return new ShowResultsEvent(numberOfPeopleToTime, myPairs);
    }

    private Long runMainAlgorithm(HashSet<MyPair> myPairs) {
        Long startTime = System.currentTimeMillis();

        final Algorithm algorithm = new Algorithm(menList, womenList);
        algorithm.runAlgorithm();
        myPairs.addAll(algorithm.getPairs());

        Long intervalTime = System.currentTimeMillis() - startTime;
        return intervalTime;
    }


    private void getRandomTestData(HashMap<RandomParameterType, Integer> randomParametersFromFrame, Integer numberOfPeople) {
        final RandomDataGenerator randomDataGenerator = new RandomDataGenerator(randomParametersFromFrame, numberOfPeople);
        menList = randomDataGenerator.getManArray();
        womenList = randomDataGenerator.getWomenArray();

    }

    private void getTestDataFromFile(HashMap<Boolean, String> shouldReadFromFile) {
        final String path = shouldReadFromFile.values().iterator().next();
        final FileParser fileParser = new FileParser(path);
        menList = fileParser.getMenList();
        womenList = fileParser.getWomenList();
    }


}