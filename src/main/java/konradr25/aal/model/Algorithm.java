package main.java.konradr25.aal.model;

import com.google.common.collect.Lists;
import main.java.konradr25.aal.model.people.Man;
import main.java.konradr25.aal.model.people.MyPair;
import main.java.konradr25.aal.model.people.Person;
import main.java.konradr25.aal.model.people.PersonComparator;
import main.java.konradr25.aal.model.people.Woman;

import javax.swing.*;
import java.util.Collections;
import java.util.List;

public class Algorithm {
    private List menList;
    private List womenList;

    private List <Person> allInOneSource;
    private List <MyPair> pairs;

    public Algorithm(List<Man> menList, List<Woman> womenList) {
        this.menList = menList;
        this.womenList = womenList;

        allInOneSource = Lists.newLinkedList();
        pairs = Lists.newLinkedList();
        allInOneSource.addAll(menList);
        allInOneSource.addAll(womenList);

        Collections.sort(allInOneSource, new PersonComparator());
    }

    public void runAlgorithm() {
        List <Person>tempWomenList = Lists.newLinkedList();

        for (Person person : allInOneSource) {
            if (!person.getMan()) {
                tempWomenList.add(person);
            } else {
                findWomanForMan(tempWomenList, person);
            }
        }

        if (tempWomenList.size() != 0 ) {
            JOptionPane.showMessageDialog(null, "There is no assignment.");
        }
    }

    private void findWomanForMan(List<Person> tempWomenList, Person person) {
        if (!tempWomenList.isEmpty()) {
            Person foundWoman = goBackwardAndFindBestWomanCandidate(tempWomenList, person);

            if (foundWoman != null) {
                pairs.add(new MyPair(person, foundWoman));
                tempWomenList.remove(foundWoman); //We made pair so we can remove found woman.
            } else {
                tryToUnhookAndReplace(tempWomenList, person);
            }

        } else {
            JOptionPane.showMessageDialog(null, "There is no assignment.");
        }
    }

    private void tryToUnhookAndReplace(List<Person> tempWomenList, Person person) {
        for (MyPair myPair : pairs) {
            boolean canAssigneWoman = myPair.getWoman().getHead() <= person.getHead() && myPair.getWoman().getHeight() <= person.getHeight();
            if (canAssigneWoman) {
                final Person foundCandidate = goBackwardAndFindBestWomanCandidate(tempWomenList, myPair.getMan());
                if (foundCandidate !=null) {
                    pairs.remove(myPair);
                    pairs.add(new MyPair(person, myPair.getWoman()));
                    pairs.add(new MyPair(myPair.getMan(), foundCandidate));
                }
            }
        }
    }

    private Person goBackwardAndFindBestWomanCandidate(List<Person> tempWomenList, Person man) {
        int i = tempWomenList.size() - 1;
        double bestDistance = Double.MAX_VALUE;
        double newBestDistance = Double.MAX_VALUE;
        Person lastWomanThatMetConditions = null;
        while (i >= 0) {
            Person woman = tempWomenList.get(i);

            final boolean pairingConditionsAreMet = woman.getHead() <= man.getHead()
                    && woman.getHeight() <= man.getHeight();

            if (pairingConditionsAreMet) {
                newBestDistance = checkTheDistance(woman, man);
                if (newBestDistance < bestDistance) {
                    bestDistance = newBestDistance;
                    lastWomanThatMetConditions = woman;
                } else {
                    return lastWomanThatMetConditions; //get the previous woman
                }
            }

            --i;
        }
        return lastWomanThatMetConditions;
    }

    private double checkTheDistance(Person woman, Person man) {
        int headDifference = man.getHead() - woman.getHead();
        int heightDifference = man.getHeight() - woman.getHeight();
        return Math.hypot(headDifference, heightDifference);
    }

    /**
     * Gets pairs.
     *
     * @return the pairs
     */
    public List<MyPair> getPairs() {
        return pairs;
    }
}
