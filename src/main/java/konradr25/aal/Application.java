package main.java.konradr25.aal;

import main.java.konradr25.aal.View.View;
import main.java.konradr25.aal.controller.Controller;
import main.java.konradr25.aal.events.ApplicationEvent;
import main.java.konradr25.aal.model.Model;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Class responsible for creating all objects that the server composes of.
 */
public class Application {

    public static void main(String[] args) {


        final BlockingQueue<ApplicationEvent> eventsBlockingQueue = new LinkedBlockingQueue<>();
        View view = new View(eventsBlockingQueue);
        Model model = new Model();
        final Controller controller = new Controller(eventsBlockingQueue, view, model);

        /* Runs threads */
        controller.start();
    }
}
