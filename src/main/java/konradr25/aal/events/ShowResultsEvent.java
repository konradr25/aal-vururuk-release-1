package main.java.konradr25.aal.events;


import main.java.konradr25.aal.model.people.MyPair;

import java.util.HashSet;
import java.util.LinkedHashMap;

public class ShowResultsEvent extends ApplicationEvent {

    private LinkedHashMap<Integer, Long> numberOfPeopleToTime;
    private HashSet<MyPair> pairs;
    public ShowResultsEvent(LinkedHashMap<Integer, Long> numberOfPeopleToTime, HashSet<MyPair> pairs) {
        this.numberOfPeopleToTime = numberOfPeopleToTime;
        this.pairs = pairs;
    }

    /**
     * Gets numberOfPeopleToTime.
     *
     * @return the numberOfPeopleToTime
     */
    public LinkedHashMap<Integer, Long> getNumberOfPeopleToTime() {
        return numberOfPeopleToTime;
    }

    /**
     * Gets pairs.
     *
     * @return the pairs
     */
    public HashSet<MyPair> getPairs() {
        return pairs;
    }
}
