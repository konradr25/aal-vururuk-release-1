package main.java.konradr25.aal.events;


import main.java.konradr25.aal.common.MainParametersType;
import main.java.konradr25.aal.common.RandomParameterType;

import java.util.HashMap;


/**
 *
 */
public class StartAlgorithmEvent extends ApplicationEvent {
    private HashMap<Boolean, String> shouldReadFromFile;

    private HashMap<RandomParameterType, Integer> randomParametersFromFrame;

    private HashMap<MainParametersType, Boolean> mainParametersFromFrame;

    public StartAlgorithmEvent(HashMap<Boolean, String> shouldReadFromFile, HashMap<RandomParameterType, Integer> randomParametersFromFrame,
                               HashMap<MainParametersType, Boolean> mainParametersFromFrame) {
        this.shouldReadFromFile = shouldReadFromFile;
        this.randomParametersFromFrame = randomParametersFromFrame;
        this.mainParametersFromFrame = mainParametersFromFrame;
    }

    /**
     * Gets randomParametersFromFrame.
     *
     * @return the randomParametersFromFrame
     */
    public HashMap<RandomParameterType, Integer> getRandomParametersFromFrame() {
        return randomParametersFromFrame;
    }

    /**
     * Sets randomParametersFromFrame.
     *
     * @param randomParametersFromFrame
     */
    public void setRandomParametersFromFrame(HashMap<RandomParameterType, Integer> randomParametersFromFrame) {
        this.randomParametersFromFrame = randomParametersFromFrame;
    }

    /**
     * Gets mainParametersFromFrame.
     *
     * @return the mainParametersFromFrame
     */
    public HashMap<MainParametersType, Boolean> getMainParametersFromFrame() {
        return mainParametersFromFrame;
    }

    /**
     * Sets mainParametersFromFrame.
     *
     * @param mainParametersFromFrame
     */
    public void setMainParametersFromFrame(HashMap<MainParametersType, Boolean> mainParametersFromFrame) {
        this.mainParametersFromFrame = mainParametersFromFrame;
    }

    /**
     * Gets shouldReadFromFile.
     *
     * @return the shouldReadFromFile
     */
    public HashMap<Boolean, String>  getShouldReadFromFile() {
        return shouldReadFromFile;
    }
}
