package main.java.konradr25.aal.controller;

import main.java.konradr25.aal.View.View;
import main.java.konradr25.aal.controller.eventstrategy.EventStrategy;
import main.java.konradr25.aal.controller.eventstrategy.ShowResultsStrategy;
import main.java.konradr25.aal.controller.eventstrategy.StartAlgorithmStrategy;
import main.java.konradr25.aal.events.ApplicationEvent;
import main.java.konradr25.aal.events.ShowResultsEvent;
import main.java.konradr25.aal.events.StartAlgorithmEvent;
import main.java.konradr25.aal.model.Model;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

/**
 * Class responsible for handling connection between the view-network-model.
 */
public class Controller {

    /** Application Modules used in controller.*/
    ApplicationModules applicationModules;

    /** Mapping ApplicationEvents to Strategy objects that handle them */
    private final Map<Class<? extends ApplicationEvent>, EventStrategy> eventsToStrategyMap;



    public Controller(final BlockingQueue<ApplicationEvent> eventsBlockingQueue,
                      final View view, final Model model) {
        this.applicationModules = new ApplicationModules(eventsBlockingQueue, view, model);

        eventsToStrategyMap = new HashMap<>();
        eventsToStrategyMap.put(ShowResultsEvent.class, new ShowResultsStrategy(applicationModules));
        eventsToStrategyMap.put(StartAlgorithmEvent.class, new StartAlgorithmStrategy(applicationModules));
    }

    /**
     * Method that listens for the blockingQueue and handles events.
     */
    public void start() {
        while (true) {
            ApplicationEvent event = null;
            try {
                event = applicationModules.getEventsBlockingQueue().take();
            }
            catch (final InterruptedException e) {
               continue;
           }
            eventsToStrategyMap.get(event.getClass()).execute(event);
        }
    }

}
