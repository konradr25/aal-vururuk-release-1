package main.java.konradr25.aal.controller.eventstrategy;

import main.java.konradr25.aal.View.ViewResult;
import main.java.konradr25.aal.View.ViewResultPairs;
import main.java.konradr25.aal.controller.ApplicationModules;
import main.java.konradr25.aal.events.ApplicationEvent;
import main.java.konradr25.aal.events.ShowResultsEvent;

/**
 * Show Results Strategy.
 */
public class ShowResultsStrategy extends EventStrategy {
    /** Application Modules used in controller. */
    private final ApplicationModules applicationModules;

    public ShowResultsStrategy(ApplicationModules applicationModules) {
        this.applicationModules = applicationModules;
    }

    @Override
    public void execute(ApplicationEvent e) {
        ShowResultsEvent event = (ShowResultsEvent) e;
        final ViewResult viewResult = new ViewResult(event.getNumberOfPeopleToTime());
        viewResult.start();

        final ViewResultPairs viewResultPairs = new ViewResultPairs(event.getPairs());
        viewResultPairs.start();
    }
}
