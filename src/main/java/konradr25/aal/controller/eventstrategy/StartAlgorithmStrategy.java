package main.java.konradr25.aal.controller.eventstrategy;

import main.java.konradr25.aal.controller.ApplicationModules;
import main.java.konradr25.aal.events.ApplicationEvent;
import main.java.konradr25.aal.events.StartAlgorithmEvent;

/**
 * Start Algorithm Strategy.
 */
public class StartAlgorithmStrategy extends EventStrategy {
    /** Application Modules used in controller. */
    private final ApplicationModules applicationModules;

    public StartAlgorithmStrategy(ApplicationModules applicationModules) {
        this.applicationModules = applicationModules;
    }

    @Override
    public void execute(ApplicationEvent e) {
        StartAlgorithmEvent event = (StartAlgorithmEvent) e;
        try {
            applicationModules.getEventsBlockingQueue().put(applicationModules.getModel().runAlgorithmModel(event));
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }

}
