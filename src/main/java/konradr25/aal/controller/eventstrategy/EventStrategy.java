package main.java.konradr25.aal.controller.eventstrategy;

import main.java.konradr25.aal.events.ApplicationEvent;

/**
 * Class that provides common base for strategies used to handle events.
 *
 */
public abstract class EventStrategy {

    /**
     * Method that is invoked in response to ApplicationEvent
     *
     * @param event ApplicationEvent to be handled
     */
    public abstract void execute(final ApplicationEvent event);
}