package main.java.konradr25.aal.controller;

import main.java.konradr25.aal.View.View;
import main.java.konradr25.aal.events.ApplicationEvent;
import main.java.konradr25.aal.model.Model;

import java.util.concurrent.BlockingQueue;

/**
 * Application Modules used in controller.
 */
public class ApplicationModules {
    /** Queue from which events are read */

    private final BlockingQueue<ApplicationEvent> eventsBlockingQueue;
    /** The view. */

    private final View view;
    /** This server's model. */

    private final Model model;

    public ApplicationModules(final BlockingQueue<ApplicationEvent> eventsBlockingQueue,
                              final View view, final Model model) {
        this.eventsBlockingQueue = eventsBlockingQueue;
        this.view = view;
        this.model = model;
    }

    /**
     * Gets eventsBlockingQueue.
     *
     * @return the eventsBlockingQueue
     */
    public BlockingQueue<ApplicationEvent> getEventsBlockingQueue() {
        return eventsBlockingQueue;
    }

    /**
     * Gets view.
     *
     * @return the view
     */
    public View getView() {
        return view;
    }

    /**
     * Gets model.
     *
     * @return the model
     */
    public Model getModel() {
        return model;
    }
}
